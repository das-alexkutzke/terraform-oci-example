# Terraform Oci Example

Exemplo de uma configuração básica para o uso da Oracle Cloud Infrastructure (OCI) com a aplicação Terraform.

Atualmente, a configuração gera N instâncias com imagens ubuntu e docker instalado.

## Uso

1. Criar uma conta na Oracle Cloud Infrastructure ([clique aqui][createaccount]);
2. Instalar o [terraform](https://learn.hashicorp.com/tutorials/terraform/install-cli?in=terraform/oci-get-started);
3. Instalar o [OCI CLI ](https://docs.oracle.com/en-us/iaas/Content/API/SDKDocs/cliinstall.htm);
4. Configurar as credenciais OCI ([oci session authenticate](https://learn.hashicorp.com/tutorials/terraform/oci-build?in=terraform/oci-get-started));
5. Clonar este repositório;
6. `terraform init`
7. `terraform apply`

## Interromper e destruir as instâncias

`terraform destroy`

[createaccount]: https://bit.ly/free-oci-dat-k8s-on-arm
